#include "MenuState.h"
#include <SDL.h>
#include "Sprite.h"
#include "SpriteManager.h"
#include "DrawManager.h"
#include <iostream> //REMOVE 
#include "InputManager.h"
#include "GameState.h"


MenuState::MenuState(System& point_extSystem)
{

	mem_extpointBackgroundSprite = nullptr;
	mem_extpointInstructionSprite = nullptr;
	mem_extSystem = point_extSystem; //Uses the argument from the constructor to an actual effect to save this struct

}


MenuState::~MenuState()
{

}

void MenuState::Enter()
{
	//Is to load the title image used for the State
	mem_extpointBackgroundSprite = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/FroggerBack.png" , 0 , 0  , 520 , 400);
	mem_extpointInstructionSprite = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/Instructions.png", 0 , 0, 330, 150);
}

void MenuState::Exit() //Destroys,  nulls and deletes all assets used in the menu state, before moving on to new states or shutting down
{
	mem_extSystem.mem_pointextSpriteManager->DestroySprite(mem_extpointInstructionSprite);
	mem_extSystem.mem_pointextSpriteManager->DestroySprite(mem_extpointBackgroundSprite);
}

bool MenuState::Update(float point_floatDeltaTime)
{
	if (mem_extSystem.mem_pointextInputManager->isKeyPressed(SDLK_RETURN))
	{
		return false;
	}
	return true; 
}

void MenuState::Draw()
{
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointBackgroundSprite, 0, 0);
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointInstructionSprite, 100, 200);
}
IState * MenuState::NextState()
{
	return new GameState(mem_extSystem);
}
