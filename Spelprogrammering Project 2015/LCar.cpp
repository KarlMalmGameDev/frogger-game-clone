#include <SDL.h>
#include "LCar.h"
#include "Sprite.h"
#include "Collider.h"

LCar::LCar(Sprite* point_extSprite, int point_intX, int point_intY, int point_intScreenWidth, int point_intScreenHeight)
{
	mem_pointextSprite = point_extSprite;
	mem_pointextCollider = new Collider(
		point_extSprite->GetRegion()->w,
		point_extSprite->GetRegion()->h);
	mem_intX = point_intX;
	mem_intY = point_intY;
	mem_intScreenWidth = point_intScreenWidth;
	mem_intScreenHeight = point_intScreenHeight;
	//mem_floatPositionHolder = 0.0f;

	mem_pointextCollider->SetParent(this);
	mem_pointextCollider->Refresh();
}

LCar::~LCar()
{

}

void LCar::Update(float point_floatDeltaTime)
{
	mem_intX += 2;

	if (mem_intX > 500)
	{
		mem_intX = -50;
	}
	mem_pointextCollider->Refresh();
}

Sprite* LCar::GetSprite()
{
	return mem_pointextSprite;
}

Collider* LCar::GetCollider()
{
	return mem_pointextCollider;
}

float LCar::GetX() { return mem_intX; }

float LCar::GetY() { return mem_intY; }

bool LCar::IsVisible() { return mem_boolIsVisible; }

EENTITYTYPE LCar::GetType() { return ENTITY_LCAR; }
