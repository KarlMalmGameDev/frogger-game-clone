#include "MusicClip.h"




MusicClip::MusicClip(Mix_Music* pointextMusic) :
	mem_pointextMusic(pointextMusic)
{
	
}


MusicClip::~MusicClip()
{

}

void MusicClip::PlayMusic(int loop)
{
	Mix_PlayMusic(mem_pointextMusic, loop);
}

void MusicClip::StopMusic()
{
	Mix_HaltMusic();
}

void MusicClip::ResumeMusic()
{
	Mix_ResumeMusic();
}

void MusicClip::Pause() //Pauses the ONLY music we have
{
	Mix_PauseMusic();
}
