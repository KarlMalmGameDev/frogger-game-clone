#pragma once
#include "Sprite.h"
#include <vector>

class AnimatedSprite : public Sprite
{
public:
	struct FrameData
	{
		SDL_Rect mem_extRegion;
		float mem_floatDuration;
	};
	AnimatedSprite(SDL_Texture* point_extTexture);

	void AddFrame(int point_intX, int point_intY,
		int point_intWidth, int point_intHeight, float point_floatDuration);

	void Update(float point_floatDeltaTime);
private:
	std::vector<FrameData> mem_arrayextFrames;
	int mem_intIndex;
	float mem_floatCurrentDuration;
};