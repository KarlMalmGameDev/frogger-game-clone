#pragma once
#include "AudioClip.h" //AudioManager and AudioClip+/MusicClip separated
#include "MusicClip.h"
#include <string>
#include <SDL_mixer.h>
#include <map>
#include <vector>


class AudioManager
{
public:
	AudioManager();
	~AudioManager();
	bool Initialize(); // Initializes audio and opens channel, checks if they're nullpointers etc, if they are, quits
	
	void Shutdown(); // Delete and clear all containers, exit audio

	AudioClip* CreateSound(const std::string& point_stringFilepath);
	MusicClip* CreateMusic(const std::string& point_stringFilePath);

	// Destroys the clips
	void DestroySound(const std::string& point_stringFilepath);
	void DestroyMusic(const std::string& point_stringFilepath);
private:
	AudioClip* mem_pointextAudioClip; 
	MusicClip* mem_pointextMusicClip; 

	//Maps for finding already loaded collections of objects/Data that's already loaded
	
	std::map<std::string, Mix_Chunk*> mem_arraypointextAudioChunks;
	std::map<std::string, Mix_Music*> mem_arraypointextMusicChunks;
	std::vector<AudioClip*> mem_arraypointextAudioClips;
	std::vector<MusicClip*> mem_arraypointextMusicClips;

};
