#include "Player.h"
#include <SDL.h>
#include "InputManager.h"
#include "Collider.h"
#include "Sprite.h"
#include "AnimatedSprite.h"



Player::Player(InputManager* point_extInputManager, Sprite* point_extSprite, float point_floatX, float point_floatY, int point_intScreenWidth, int point_intScreenHeight)
{
	mem_pointextInputManager = point_extInputManager;
	mem_pointextSprite = point_extSprite;
	mem_floatX = point_floatX;
	mem_floatY = point_floatY;
	int_screenWidth = point_intScreenWidth;
	
	int_screenHeight = point_intScreenHeight;
	mem_pointextCollider = new Collider(point_extSprite->GetRegion()->w, point_extSprite->GetRegion()->h);
	mem_pointextCollider->SetParent(this);
	mem_pointextCollider->Refresh();

}

Player::~Player()
{
	delete mem_pointextCollider;
	mem_pointextCollider = nullptr;
}

void Player::Update(float point_floatDeltaTime)
{	
	// Movement
		if (mem_pointextInputManager->isKeyPressed(SDLK_w))
		{
			mem_floatY -= 20;
			mem_intDirection = 1;
		}
	

		if (mem_pointextInputManager->isKeyPressed(SDLK_a))
		{
			mem_floatX -= 20;
			mem_intDirection = 2;
		}
	

	
		if (mem_pointextInputManager->isKeyPressed(SDLK_s))
		{
			mem_floatY += 20;
			mem_intDirection = 3;
		}

		if (mem_pointextInputManager->isKeyPressed(SDLK_d))
		{
			mem_floatX += 20;
			mem_intDirection = 4;
		}

		//Bounds
		if (mem_floatX < 0)
		{
			mem_floatX = 0;
		}

		if (mem_floatX > 500)
		{
			mem_floatX = 500;
		}

		if (mem_floatY > 380)
		{
			mem_floatY = 380;
		}
		mem_pointextCollider->Refresh();
		
	
}


Collider* Player::GetCollider()
{
	return mem_pointextCollider;
}

Sprite* Player::GetSprite()
{
	return mem_pointextSprite;
}

float Player::GetX()
{
	return mem_floatX;
}

float Player::GetY()
{
	return mem_floatY;
}

bool Player::IsVisible()
{
	return mem_floatX;
}

EENTITYTYPE Player::GetType()
{
	return ENTITY_PLAYER;
}
