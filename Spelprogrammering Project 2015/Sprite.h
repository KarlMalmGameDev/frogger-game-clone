#pragma once
#include <SDL.h>
class Sprite
{
public:
	Sprite(SDL_Texture* point_extTexture);
	Sprite(SDL_Texture* point_extTexture, int point_integerX, int point_integerY, int point_integerW, int point_integerH);
	~Sprite();
	SDL_Texture* GetTexture();
	SDL_Rect* GetRegion();

protected:
	SDL_Texture* mem_pointTexture;
	SDL_Rect mem_extRegion;
};
