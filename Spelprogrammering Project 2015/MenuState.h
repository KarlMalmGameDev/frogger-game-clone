#pragma once
class Sprite;

#include "IState.h"
#include <SDL_mixer.h>
class MenuState : public IState
{
public:
	MenuState(System& point_extSystem);
	~MenuState();
	void Enter();
	void Exit();
	bool Update(float point_floatDeltaTime);
	void Draw();
	IState* NextState();
private:
	System mem_extSystem;
	Sprite* mem_extpointBackgroundSprite;
	Sprite* mem_extpointInstructionSprite;
};

