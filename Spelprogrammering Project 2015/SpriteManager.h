#pragma once
#include <SDL.h>
#include <string>
#include <vector>
#include <map>
class Sprite;
class AnimatedSprite;

class SpriteManager
{
public:
	SpriteManager(SDL_Renderer* point_extRenderer);
	~SpriteManager();

	Sprite* CreateSprite(const std::string& point_stringFilePath, int point_intX, int point_intY, int point_intWidth, int point_intHeight);
	AnimatedSprite* CreateAnimatedSprite(const std::string& point_stringFilepath);
	void DestroySprite(Sprite* point_extSprite);
private:
	SpriteManager();
	SDL_Renderer* mem_pointextRenderer;
	std::vector<Sprite*> mem_arraypointextSprites;
	std::map<std::string, SDL_Texture*> mem_arraypointextTextures;
};