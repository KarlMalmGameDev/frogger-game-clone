#pragma once

class Collider;

class CollisionManager
{
public:
	// Static means the function belongs to the class and can be reached without having an
	// object of the type CollisionManager.
	static bool Check(Collider* point_extLeft, Collider* point_extRight, int& point_intOverlapX, int& point_intOverlapY);
};