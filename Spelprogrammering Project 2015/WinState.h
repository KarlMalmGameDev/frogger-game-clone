#pragma once
class Sprite;
class MusicClip;
#include "IState.h"

class WinState : public IState
{
public:
	WinState(System& pointextSystem);
	~WinState();
	void Enter();
	void Exit();
	bool Update(float point_floatDeltaTime);
	void Draw();
	IState* NextState();
private:
	System mem_extSystem;
	Sprite* mem_extpointBackgroundSprite;
	MusicClip* mem_extpointWinMusic;
};