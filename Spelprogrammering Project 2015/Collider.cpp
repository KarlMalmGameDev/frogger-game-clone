#include "Collider.h"
#include <SDL.h>

#include "Collider.h"
#include "IEntity.h"

Collider::Collider(int point_intWidth,
	int point_intHeight)
{
	mem_extRegion.x = 0;
	mem_extRegion.y = 0;
	mem_extRegion.w = point_intWidth;
	mem_extRegion.h = point_intHeight;
	mem_pointextParent = nullptr;
}
void Collider::SetPosition(int point_intX,
	int point_intY)
{
	mem_extRegion.x = point_intX;
	mem_extRegion.y = point_intY;
}
void Collider::SetSize(int point_intWidth,
	int point_intHeight)
{
	if (point_intWidth < 0)
		point_intWidth = 0;
	if (point_intHeight < 0)
		point_intHeight = 0;
	mem_extRegion.w = point_intWidth;
	mem_extRegion.h = point_intHeight;
}
int Collider::GetX()
{
	return mem_extRegion.x;
};
int Collider::GetY()
{
	return mem_extRegion.y;
}
int Collider::GetW()
{
	return mem_extRegion.w;
}
int Collider::GetH()
{
	return mem_extRegion.h;
}

void Collider::SetParent(IEntity* p_pxParent)
{
	mem_pointextParent = p_pxParent;
}

void Collider::Refresh()
{
	if (mem_pointextParent == nullptr)
		return;
	mem_extRegion.x = mem_pointextParent->GetX();
	mem_extRegion.y = mem_pointextParent->GetY();
}