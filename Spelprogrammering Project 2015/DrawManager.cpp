#include "DrawManager.h"
#include "Sprite.h"

//Unfinished?


DrawManager::DrawManager()
{

}


DrawManager::~DrawManager()
{

}

bool DrawManager::isInitialized(int point_Width, int point_Height) //Uses paramemers to create an SDL window
{
	mem_pointextWindow = SDL_CreateWindow("Frogger Clone",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		point_Width, point_Height, SDL_WINDOW_OPENGL);

	if (mem_pointextWindow == nullptr) // If the method produces a nullpointer, an error has occured and the program quits
	{
		return false;
	}

	//Creates a renderer attached to the window the program just created
	mem_pointextRenderer = SDL_CreateRenderer(mem_pointextWindow, -1, SDL_RENDERER_ACCELERATED);
	
	if (mem_pointextRenderer == nullptr) //If the method produces a nullpointer, an error has occured and the program quits
	{
		const char* error = SDL_GetError();
		return false;
	}

	//Sets the drawcolor to clear the window's screen with. Returns true if all code executed properly
	SDL_SetRenderDrawColor(mem_pointextRenderer, 0x00, 0x00, 0x00, 0xff);
	return true;
}

void DrawManager::Shutdown()
{
	SDL_DestroyRenderer(mem_pointextRenderer);

	SDL_DestroyWindow(mem_pointextWindow);
}

void DrawManager::Clear()
{
	//Clears the drawn content in the Renderer
	SDL_RenderClear(mem_pointextRenderer);
}

void DrawManager::Present() //Updates with all new rendering performed
{
	SDL_RenderPresent(mem_pointextRenderer);
}

void DrawManager::Draw(Sprite * point_extSprite, int point_integerX, int point_integerY) 
{
	SDL_Rect rectangle = { point_integerX, point_integerY, point_extSprite->GetRegion()->w, point_extSprite->GetRegion()->h };

	SDL_RenderCopy(mem_pointextRenderer, point_extSprite->GetTexture(),
		point_extSprite->GetRegion(), &rectangle);
}

SDL_Renderer * DrawManager::GetRenderer()
{
	return mem_pointextRenderer;
}
