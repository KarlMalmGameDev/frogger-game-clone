#pragma once
#include <SDL.h>

class Sprite;

class DrawManager
{
public:
	DrawManager();
	~DrawManager();

	bool isInitialized(int point_Width, int point_Height);
	void Shutdown();
	void Clear();
	void Present();
	void Draw(Sprite* point_extSprite, int point_integerX, int point_integerY);

	SDL_Renderer* GetRenderer();
private:
	SDL_Window* mem_pointextWindow = nullptr;
	SDL_Renderer* mem_pointextRenderer = nullptr;
};



