#pragma once
#include <string>
#include <SDL_mixer.h>
#include <map>
#include <vector>
class AudioClip
{
public:
	AudioClip(Mix_Chunk* point_extChunk );
	~AudioClip();

	void PlaySound();
private:
	Mix_Chunk* mem_pointextClip;
	int mem_intLastChannelPlayed;
};