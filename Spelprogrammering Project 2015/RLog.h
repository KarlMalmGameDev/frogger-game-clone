#pragma once
#include "IEntity.h"
class Player;

class RLog: public IEntity
{
public:
	RLog(Sprite* point_extSprite, int point_intX, int point_intY, int intScreenWidth, int intScreenHeight	);
	~RLog();
	void Update(float point_floatDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();
	float mem_floatPositionHolder;

private:
	RLog() { };
	Sprite* mem_pointextSprite;
	Collider* mem_pointextCollider;
	Player* mem_pointextPlayer;
	int mem_intX;
	int mem_intY;
	bool mem_boolVisible;
	int mem_intScreenWidth;
	int mem_intScreenHeight;
};
