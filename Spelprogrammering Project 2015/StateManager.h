#pragma once// NEEDS MORE CODE
class IState;
class StateManager
{
	friend class Engine;
public:
	StateManager();
	~StateManager();
	bool Update();
	void Draw();
private:
	void SetState(IState* point_extState);
	IState* mem_pointextCurrentState;
	int mem_intLastTick;
};

