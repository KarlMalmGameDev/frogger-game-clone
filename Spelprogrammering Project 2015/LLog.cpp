#include <SDL.h>
#include "LLog.h"
#include "Sprite.h"
#include "Collider.h"

LLog::LLog(Sprite* point_extSprite, float point_floatX, float point_floatY, int intScreenWidth, int intScreenHeight)
{
	mem_pointextSprite = point_extSprite;
	mem_floatX = point_floatX;
	mem_floatY = point_floatY;
	mem_intScreenWidth = intScreenWidth;
	mem_intScreenHeight = intScreenHeight;
	mem_pointextCollider = new Collider(
		point_extSprite->GetRegion()->w,
		point_extSprite->GetRegion()->h);
	mem_pointextCollider->SetParent(this);
	mem_pointextCollider->Refresh();
	mem_floatPositionHolder = 0.0f;
}

LLog::~LLog()
{

}

void LLog::Update(float point_floatDeltaTime)
{
	if (mem_floatY == 175.0f)
	{
		mem_floatX -= 1.0f;
		mem_floatspeed = 1.0f;
	}
	if (mem_floatY == 140.0f)
	{
		mem_floatX -= 1.1f;
		mem_floatspeed = 1.1f;
	}

	if (mem_floatY == 105.0f)
	{
		mem_floatX -= 1.2f;
		mem_floatspeed = 1.2f;
	}

	if (mem_floatX < -80)
	{
		mem_floatX = 520;
	}

	mem_pointextCollider->Refresh();
}

Sprite* LLog::GetSprite()
{
	return mem_pointextSprite;
}

Collider* LLog::GetCollider()
{
	return mem_pointextCollider;
}

float LLog::GetX() { return mem_floatX; }

float LLog::GetY() { return mem_floatY; }

float LLog::GetSpeed() { return mem_floatspeed; }

bool LLog::IsVisible() { return mem_boolVisible; }

EENTITYTYPE LLog::GetType() { return ENTITY_LLOG; }
