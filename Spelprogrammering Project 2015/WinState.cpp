#include "WinState.h"
#include <SDL.h>
#include "IState.h"
#include "InputManager.h"
#include "Sprite.h"
#include "SpriteManager.h"
#include "DrawManager.h"
#include "AudioManager.h"
#include "MusicClip.h"

WinState::WinState(System & point_extSystem)
{
	mem_extpointBackgroundSprite = nullptr;
	mem_extpointWinMusic = nullptr;
	mem_extSystem = point_extSystem;
}

WinState::~WinState()
{
}

void WinState::Enter()
{
	mem_extpointWinMusic = mem_extSystem.mem_pointextAudioManager->CreateMusic("../assets/GameWin.mp3");
	mem_extpointBackgroundSprite = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/FroggerWin.bmp", 0, 0, 520, 400);
	mem_extpointWinMusic->PlayMusic(-1);
}

void WinState::Exit()
{
	mem_extpointWinMusic->StopMusic();
	mem_extSystem.mem_pointextAudioManager->DestroyMusic("../assets/GameWin.wav");
	mem_extSystem.mem_pointextSpriteManager->DestroySprite(mem_extpointBackgroundSprite);
}

bool WinState::Update(float point_floatDeltaTime)
{
	
	return true;
}

void WinState::Draw()
{
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointBackgroundSprite, 0, 0);
}

IState * WinState::NextState()
{
	return nullptr;
}
