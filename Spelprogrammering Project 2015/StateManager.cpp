#include "StateManager.h"
#include <SDL.h>
#include "Istate.h"
//Check if complete


StateManager::StateManager()
{
	mem_pointextCurrentState = nullptr;
	mem_intLastTick = SDL_GetTicks();
}


StateManager::~StateManager()
{
	//Checks if not a null state, if not, nulls and Exits
	if (mem_pointextCurrentState != nullptr)
	{
		mem_pointextCurrentState->Exit();
		delete mem_pointextCurrentState;
		mem_pointextCurrentState = nullptr;
	}
}

bool StateManager::Update()
{
	//Measures current tick against last update	
	float fDeltaTime =
		(SDL_GetTicks() - mem_intLastTick)
		* 0.001f;


	if (mem_pointextCurrentState != nullptr)
	{
		if (mem_pointextCurrentState->Update(fDeltaTime) == false)
		{
			SetState(mem_pointextCurrentState->NextState());
		}
	}

	// Saves current tick number for future Update
	mem_intLastTick = SDL_GetTicks();
	return true;
}

void StateManager::Draw()
{
	if (mem_pointextCurrentState != nullptr)
	{
		mem_pointextCurrentState->Draw();
	}
}

void StateManager::SetState(IState * point_extState)
{
	// If the state isn't nulled, run exit and null the state
	if (mem_pointextCurrentState != nullptr)
	{
		mem_pointextCurrentState->Exit();
		delete mem_pointextCurrentState;
	}
	// Sets the current state to be the state sent as paramemer and 
	// calls the Enter function on the current state.
	mem_pointextCurrentState = point_extState;
	mem_pointextCurrentState->Enter();
}
