#pragma once
#include "IState.h"
class DrawManager;
class SpriteManager;
class StateManager;
class InputManager;
class AudioManager;


class Engine
{
public:
	Engine();
	~Engine();
	bool Initialize();
	void Shutdown();
	void Run();
	void HandleEvents();
private:
	bool mem_boolRunning;
	DrawManager* mem_pointextDrawManager;
	SpriteManager* mem_pointextSpriteManager;
	StateManager* mem_pointextStateManager;
	InputManager* mem_pointextInputManager;
	AudioManager* mem_pointextAudioManager; 
	System mem_extGame;
};

