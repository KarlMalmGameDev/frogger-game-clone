#include "SpriteManager.h"
#include "Sprite.h"
#include "AnimatedSprite.h"
#include <SDL.h>
#include <SDL_image.h>

SpriteManager::SpriteManager(SDL_Renderer * point_extRenderer)
{
	mem_pointextRenderer = point_extRenderer;
}

SpriteManager::~SpriteManager()
{
	{
		//Creates an iterator that begins at the start of the array, goes through it until the end,
		//deleting the memory points of each element
		auto iter = mem_arraypointextSprites.begin();
		while (iter != mem_arraypointextSprites.end())
		{
			delete (*iter);
			iter++;
		}
		mem_arraypointextSprites.clear();
	}
	{
		//uses the SDL_Destroy function  to repeat the above action for textures
		auto iter = mem_arraypointextTextures.begin();
		while (iter != mem_arraypointextTextures.end())
		{
			SDL_DestroyTexture(iter->second);
			iter++;
		}
		mem_arraypointextTextures.clear();
	}
}

Sprite * SpriteManager::CreateSprite(const std::string & point_stringFilePath, int point_intX, int point_intY, int point_intWidth, int point_intHeight)
{
	auto iter = mem_arraypointextTextures.find(point_stringFilePath);
	if (iter == mem_arraypointextTextures.end())
		//If the iterator cannot locate the sprite we need in our already loaded memory, 
		//it needs to be loaded into our map to create pointers 
	{
		SDL_Surface* extSurface = IMG_Load(point_stringFilePath.c_str());
		SDL_Texture* extTexture = SDL_CreateTextureFromSurface(mem_pointextRenderer, extSurface);
		SDL_FreeSurface(extSurface);
		mem_arraypointextTextures.insert(std::pair<std::string, SDL_Texture*>(point_stringFilePath, extTexture));
		iter = mem_arraypointextTextures.find(point_stringFilePath);

	}
	//Creates the sprite, adds a new index point via pushback
	Sprite* extSprite = new Sprite(iter->second, point_intX, point_intY, point_intWidth, point_intHeight);
	mem_arraypointextSprites.push_back(extSprite);
	return extSprite;
}

AnimatedSprite* SpriteManager::CreateAnimatedSprite(const std::string& point_stringFilepath)
{
	auto iter = mem_arraypointextTextures.find(point_stringFilepath);
	if (iter == mem_arraypointextTextures.end())
	{
		// If we do not find the texture we need to load it and inser it in to our std::map so
		// that we may create pointers to the same texture for several sprites.
		SDL_Surface* xSurface = IMG_Load(point_stringFilepath.c_str());
		SDL_Texture* xTexture = SDL_CreateTextureFromSurface(mem_pointextRenderer, xSurface);
		SDL_FreeSurface(xSurface);
		mem_arraypointextTextures.insert(std::pair<std::string, SDL_Texture*>(point_stringFilepath, xTexture));
		iter = mem_arraypointextTextures.find(point_stringFilepath);
	}

	AnimatedSprite* xSprite = new AnimatedSprite(iter->second);
	mem_arraypointextSprites.push_back(xSprite);
	return xSprite;
}

void SpriteManager::DestroySprite(Sprite * point_extSprite)
{
	//function that deletes/erases Sprite data by iterating through pointers in an index
	//then stops one the appropriate Sprite is found and deletes it
	auto iter = mem_arraypointextSprites.begin();
	while (iter != mem_arraypointextSprites.end())
	{
		if ((*iter) == point_extSprite)
		{
			delete (*iter);
			mem_arraypointextSprites.erase(iter);
			return;
		}
		iter++;
	}
}