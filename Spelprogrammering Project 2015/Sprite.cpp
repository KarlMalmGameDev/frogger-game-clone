#include "Sprite.h"
#include <SDL.h>


Sprite::Sprite(SDL_Texture* point_extTexture)
{
	mem_pointTexture = point_extTexture;
	mem_extRegion.x = 0;
	mem_extRegion.y = 0;
	mem_extRegion.w = 0;
	mem_extRegion.h = 0;
}

Sprite::Sprite(SDL_Texture* point_extTexture, int point_integerX, int point_integerY, int point_integerW, int point_integerH)
{
	mem_pointTexture = point_extTexture;
	mem_extRegion.x = point_integerX;
	mem_extRegion.y = point_integerY;
	mem_extRegion.w = point_integerW;
	mem_extRegion.h = point_integerH;
}

SDL_Texture* Sprite::GetTexture()
{
	return mem_pointTexture;
}

SDL_Rect* Sprite::GetRegion()
{
	return &mem_extRegion;
}

Sprite::~Sprite()
{
}
