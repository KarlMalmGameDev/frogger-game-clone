#include <SDL.h>
#include "CollisionManager.h"
#include "Collider.h"
#include <math.h>

bool CollisionManager::Check(Collider* point_extLeft, Collider* point_extRight,
	int& point_intOverlapX, int& point_intOverlapY)
{
	point_intOverlapX = 0;
	point_intOverlapY = 0;
	// Separated Axis Theorem
	int intLeftCenterX = point_extLeft->GetX() + point_extLeft->GetW() / 2; // Calculate center position left X
	int intLeftCenterY = point_extLeft->GetY() + point_extLeft->GetH() / 2; // Calculate center position left y
	int intRightCenterX = point_extRight->GetX() + point_extRight->GetW() / 2; // Calculate center position right x
	int intRightCenterY = point_extRight->GetY() + point_extRight->GetH() / 2; // Calculate center position right y
	int intCenterDeltaX = intLeftCenterX - intRightCenterX; // Distance between center X left and right
	int intCenterDeltaY = intLeftCenterY - intRightCenterY; // Distance between center Y left and right

													  // If distance between the rectangles center in X axis is LESS then their half their combined WIDTH they are overlapping in X axis. 
	if (abs(intCenterDeltaX) < (point_extLeft->GetW() / 2 + point_extRight->GetW() / 2))
	{
		// If distance between the rectangles center in Y axis is LESS then their half their combined HEIGHT they are overlapping in Y axis. 
		if (abs(intCenterDeltaY) < (point_extLeft->GetH() / 2 + point_extRight->GetH() / 2))
		{
			int intDeltaX = (point_extLeft->GetW() / 2 + point_extRight->GetW() / 2) - abs(intCenterDeltaX);
			int intDeltaY = (point_extLeft->GetH() / 2 + point_extRight->GetH() / 2) - abs(intCenterDeltaY);

			// Find the smallest overlap and return that information in the ints passed as reference
			if (intDeltaY < intDeltaX)
			{
				point_intOverlapY = intDeltaY;
				if (intCenterDeltaY < 0)
					point_intOverlapY = -point_intOverlapY;
			}
			else if (intDeltaY > intDeltaX)
			{
				point_intOverlapX = intDeltaX;
				if (intCenterDeltaX < 0)
					point_intOverlapX = -point_intOverlapX;
			}
			else
			{
				if (intCenterDeltaX < 0)
					point_intOverlapX = -intDeltaX;
				else
					point_intOverlapX = intDeltaX;


				if (intCenterDeltaY < 0)
					point_intOverlapY = -intDeltaY;
				else
					point_intOverlapY = intDeltaY;

			}
			return true;
		}
	}

	return false;
}