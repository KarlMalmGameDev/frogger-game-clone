#include "InputManager.h"


InputManager::InputManager() : memb_mousePosition(0.0f)
{

}

InputManager::~InputManager()
{

}



bool InputManager::isKeyDown(unsigned int key)
{
	//Creates an iterator that goes through
	//the number of possible keys in our map
	//If the key cannot be found, set false
	auto it = memb_keyPress.find(key);
	//Checks if the iterator is not pointing at the map's end
	if (it != memb_keyPress.end())
	{
		return it->second; //Returns the bool being pointed at in the map
	}
	return false;
}

bool InputManager::isKeyPressed(unsigned int key)
{
	if (isKeyDown(key) && !wasKeyPressed(key))
	{
		return true;
	}
	return false;
}

void InputManager::setKeyPressed(unsigned int key)
{
	memb_keyPress[key] = true;
}

void InputManager::setKeyReleased(unsigned int key)
{
	memb_keyPress[key] = false;
}

void InputManager::Update()
{
	//Goes through each key stored in the keymap keyPress,
	//sets them as previously pressed if true
	for (auto &it : memb_keyPress) 
	{
		memb_previouslyPressed[it.first] = it.second;
	}
}
//Not used in this code, can be used in future projects
void InputManager::setMousePosition(glm::vec2 position)
{
	memb_mousePosition = position;
}

bool InputManager::wasKeyPressed(unsigned int key) //Checks if a key has been pressed since last update
{
	auto it = memb_previouslyPressed.find(key);

	if (it != memb_previouslyPressed.end())
	{
		return it->second; 
	}
	return false;
}

