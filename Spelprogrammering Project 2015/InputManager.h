#pragma once
#include <glm.hpp> //External C++ Open GL library, found at glm.g-truc.net/0.9.7/index.html
#include <unordered_map> 

class InputManager
{
public:
	InputManager();
	~InputManager();
	void Update();

	bool isKeyDown(unsigned int key);
	bool isKeyPressed(unsigned int key);
	//Setters
	void setKeyPressed(unsigned int key);
	void setKeyReleased(unsigned int key);
	void setMousePosition(glm::vec2 position);

	//Getter
	glm::vec2 getMousePosition() const { return memb_mousePosition; }
	

private:
	bool wasKeyPressed(unsigned int key);
	//Mouse Position GLM
	glm::vec2 memb_mousePosition;
	//	Map for keyboard presses
	std::unordered_map<unsigned int, bool> memb_keyPress;
	//Map for previous keys
	std::unordered_map<unsigned int, bool> memb_previouslyPressed;
}; 