#pragma once
#include "IEntity.h"
class InputManager;
class AnimatedSprite;
class Sprite;

class Player : public IEntity
{
public:
	Player(InputManager* point_extInputManager, Sprite* point_extSprite,
	float point_floatX, float point_floatY, int point_intScreenWidth, int point_intScreenHeight);
	~Player();
	void Update(float point_floatDeltaTime);
	Collider* GetCollider();
	Sprite* GetSprite();
	int mem_intDirection;
	int mem_intOneUps = 5;
	float mem_floatX;
	float mem_floatY;
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();

private:
	Player() {};
	Sprite* mem_pointextSprite;
	Collider* mem_pointextCollider;
	InputManager* mem_pointextInputManager;
	float mem_floatDelay;
	int int_screenWidth;
	int int_screenHeight;
	bool mem_boolVisible;
};

