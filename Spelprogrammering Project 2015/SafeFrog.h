#pragma once
class Player;
#include "IEntity.h"
class SafeFrog : public IEntity
{
public:
	SafeFrog(Sprite* point_extSprite, int point_intX, int point_intY, int intScreenWidth, int intScreenHeight);
	~SafeFrog();
	void Update(float point_floatDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	void SetVisible(bool priv_boolvalue);
	EENTITYTYPE GetType();

private:
	SafeFrog() {};
	Sprite* mem_pointextSprite;
	Collider* mem_pointextCollider;
	Player* mem_pointextPlayer;
	int mem_intX;
	int mem_intY;
	bool mem_boolVisible;
	int mem_intScreenWidth;
	int mem_intScreenHeight;
};

