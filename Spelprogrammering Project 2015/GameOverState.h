#pragma once
class Sprite;
class AudioClip;
#include "IState.h"
class GameOverState: public IState
{
public:
	GameOverState(System& point_extSystem);
	~GameOverState();
	void Enter();
	void Exit();
	bool Update(float point_floatDeltaTime);
	void Draw();
	IState* NextState();
private:
	System mem_extSystem;
	AudioClip* mem_extpointLoseAudio;
	Sprite* mem_extpointBackgroundSprite;
};

