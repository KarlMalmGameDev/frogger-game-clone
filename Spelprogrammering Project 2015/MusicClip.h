#pragma once
#include "SDL_Mixer.h"
class MusicClip
{
public:
	MusicClip(Mix_Music* point_extMusic);
	~MusicClip();
	void PlayMusic(int loop=-1); //Default value of -1 for infinite looping
	void StopMusic();
	void ResumeMusic();
	void Pause();
private:
	Mix_Music* mem_pointextMusic;
};

