#pragma once 
#include "Istate.h"
#include <SDL.h>
#include <vector>
#include "RLog.h"
#include "LLog.h"
#include "SafeFrog.h"
#include "LCar.h"


class AudioClip;
class MusicClip;
class AudioManager;
class Player;
class InputManager;
class Sprite;
class AnimatedSprite;
class LLog;
class LCar;
class SafeFrog;

class GameState : public IState
{
public:
	GameState(System& point_extSystem);
	~GameState();
	void Enter();
	void Exit();
	bool Update(float point_floatDeltaTime);
	void Draw();
	IState* NextState();
private:
	int frogsLeft;
	bool isDrowned = false;
	void CheckCollision();
	System mem_extSystem;


	Player* mem_extpointPlayer;

	//Sprites

	Sprite* mem_extpointBackgroundSprite;
	Sprite* mem_extpointLogSprite;
	Sprite* mem_extpointPlayerFrogSprite;
	Sprite* mem_extpointPlayerFrogSprite1;
	Sprite* mem_extpointPlayerFrogSprite2;
	Sprite* mem_extpointPlayerFrogSprite3;
	Sprite* mem_extpointOneUpSprite;
	Sprite* mem_extpointLCarSprite;
	Sprite* mem_extpointLCarSprite1;
	Sprite* mem_extpointLCarTruckSprite;
	Sprite* mem_extpointSafeFrogSprite;

	std::vector <SDL_Rect*> mem_extvectRects;

	//RLogs
	std::vector <RLog> mem_extvectRLog;
	int rlogx=-100, rlogy=80;
	/*
	RLog* mem_extpointRightLog;
	RLog* mem_extpointRightLog1;
	RLog* mem_extpointRightLog2;
	RLog* mem_extpointRightLog3;
	RLog* mem_extpointRightLog4;
	RLog* mem_extpointRightLog5;
	RLog* mem_extpointRightLog6;
	RLog* mem_extpointRightLog7;
	RLog* mem_extpointRightLog8;
	RLog* mem_extpointRightLog9;
	RLog* mem_extpointRightLog10;
	RLog* mem_extpointRightLog11;
	RLog* mem_extpointRightLog12;
	RLog* mem_extpointRightLog13;
	RLog* mem_extpointRightLog14;
	RLog* mem_extpointRightLog15;
	RLog* mem_extpointRightLog16;
	RLog* mem_extpointRightLog17;
	*/
	//LLogs
	std::vector <LLog> mem_extvectLLogs;
	int llogx = 600, llogy = 180;
	/*
	LLog* mem_extpointLeftLog;
	LLog* mem_extpointLeftLog1;
	LLog* mem_extpointLeftLog2;
	LLog* mem_extpointLeftLog3;
	LLog* mem_extpointLeftLog4;
	LLog* mem_extpointLeftLog5;
	LLog* mem_extpointLeftLog6;
	LLog* mem_extpointLeftLog7;
	LLog* mem_extpointLeftLog8;
	LLog* mem_extpointLeftLog9;
	LLog* mem_extpointLeftLog10;
	LLog* mem_extpointLeftLog11;
	LLog* mem_extpointLeftLog12;
	LLog* mem_extpointLeftLog13;
	LLog* mem_extpointLeftLog14;
	LLog* mem_extpointLeftLog15;
	LLog* mem_extpointLeftLog16;
	LLog* mem_extpointLeftLog17;
	*/
	//LCars 
	std::vector <LCar> mem_extvectLCars;
	int sportsx = 300, sportsy = 230, regx=50, regy=260, truckx=0, trucky=330;
	/*
	LCar* mem_extpointLeftCar;
	LCar* mem_extpointLeftCar1;
	LCar* mem_extpointLeftCar2;
	LCar* mem_extpointLeftCar3;
	LCar* mem_extpointLeftCar4;
	LCar* mem_extpointLeftCar5;
	LCar* mem_extpointLeftCar6;
	LCar* mem_extpointLeftCar7;
	LCar* mem_extpointLeftCar8;
	*/
	//SafeFrogs
	std::vector <SafeFrog> mem_extvectSafeFrogs;
	int safex = 25, safey = 50;
	/*
	SafeFrog* mem_extpointSafeFrog;
	SafeFrog* mem_extpointSafeFrog1;
	SafeFrog* mem_extpointSafeFrog2;
	SafeFrog* mem_extpointSafeFrog3;
	SafeFrog* mem_extpointSafeFrog4;
	*/


	//Audio Clips
	AudioClip* mem_extpointSplatClip; 
	AudioClip* mem_extpointCollisionClip;
	AudioClip* mem_extpointDrowningClip;
	AudioClip* mem_extpointGoalClip;

	//Music Clips
	MusicClip* mem_extpointTheme;
};

