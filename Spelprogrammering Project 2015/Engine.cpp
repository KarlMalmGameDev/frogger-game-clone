//Lots of these don't exist yet, need to be made
#include "Engine.h"
#include "DrawManager.h"
#include "SpriteManager.h"
#include "StateManager.h"
#include "AudioManager.h"
#include "Sprite.h"
#include "InputManager.h"
#include "GameState.h"
#include "IState.h"
#include <iostream>
#include <ctime>
#include "MenuState.h"
#include <SDL.h>
#include "GameOverState.h"
#include <SDL_image.h>
#include <string.h>

const int ScreenWidth = 520;
const int ScreenHeight = 400;
int menu = 0;
Engine::Engine()
{
	mem_boolRunning = false;
	mem_pointextDrawManager = nullptr;
	mem_pointextSpriteManager = nullptr;
	mem_pointextStateManager = nullptr;
	mem_pointextInputManager = nullptr;

}


Engine::~Engine()
{
}

bool Engine::Initialize()
{
	srand((unsigned int)time(0)); //Creates a random seed

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) //Initiates the SDL library, if not available , closes the program
	{
		return false;
	}

	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		return false;
	}
	mem_pointextDrawManager = new DrawManager();
	if (mem_pointextDrawManager->isInitialized(ScreenWidth, ScreenHeight) == false)
	{
		return false;
	}
	mem_pointextInputManager = new InputManager();
	mem_pointextSpriteManager = new SpriteManager(mem_pointextDrawManager->GetRenderer());

	mem_pointextStateManager = new StateManager();

	mem_pointextAudioManager = new AudioManager(); //Newly Added Code


	mem_pointextAudioManager->Initialize();

	mem_extGame.mem_intScreenWidth = ScreenWidth;
	mem_extGame.mem_intScreenHeight = ScreenHeight;
	mem_extGame.mem_pointextDrawManager = mem_pointextDrawManager;
	mem_extGame.mem_pointextSpriteManager = mem_pointextSpriteManager;
	mem_extGame.mem_pointextInputManager = mem_pointextInputManager;
	mem_extGame.mem_pointextAudioManager = mem_pointextAudioManager; //Newly Added Code

	mem_pointextStateManager->SetState(new MenuState(mem_extGame));

	mem_boolRunning = true;
	return true;
}

void Engine::Shutdown()
{
	// The shutdown function will quit, delete and shutdown everything we have started up or created in initialize (In reverse order of creation)
	mem_pointextAudioManager->Shutdown(); //Newly Added code
	delete mem_pointextAudioManager;
	mem_pointextAudioManager = nullptr;
	
	delete mem_pointextStateManager;
	mem_pointextStateManager = nullptr;
	
	delete mem_pointextSpriteManager;
	mem_pointextSpriteManager = nullptr;

	delete mem_pointextInputManager;
	mem_pointextInputManager = nullptr;

	// Shuts down the drawmanager before deleting the object and nulling the pointer.
	mem_pointextDrawManager->Shutdown();
	delete mem_pointextDrawManager;
	mem_pointextDrawManager = nullptr;

	SDL_Quit();
}



void Engine::Run()
{
	while (mem_boolRunning)
	{
		HandleEvents();
		mem_pointextDrawManager->Clear();
		if (mem_pointextStateManager->Update() == false)
		{
			mem_boolRunning = false;
		}
		mem_pointextStateManager->Draw();
		mem_pointextDrawManager->Present();
		SDL_Delay(10);

		if (mem_pointextInputManager->isKeyPressed(SDLK_ESCAPE))
		{
			SDL_QUIT;
			mem_boolRunning = false;
		}
	}
}

void Engine::HandleEvents() 
{
	mem_pointextInputManager->Update();
	SDL_Event evnt;

	while (SDL_PollEvent(&evnt))
	{
		switch (evnt.type)
		{
		case SDL_QUIT:
			//Should quit the game
			break;

		case SDL_KEYDOWN:
			mem_pointextInputManager->setKeyPressed(evnt.key.keysym.sym);
			break;
		case SDL_KEYUP:
			mem_pointextInputManager->setKeyReleased(evnt.key.keysym.sym);
			break;
		}
		
	}

}
