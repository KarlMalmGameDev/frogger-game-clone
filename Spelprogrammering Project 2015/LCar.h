#pragma once
#include "IEntity.h"

class LCar :public IEntity
{
public:
	LCar(Sprite* point_extSprite, int point_intX, int point_intY, int point_intScreenWidth, int point_intScreenHeight);
	~LCar();
	void Update(float point_floatDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();

private:
	LCar() {};
	Sprite* mem_pointextSprite;
	Collider* mem_pointextCollider;
	bool mem_boolIsVisible;

	// Remove float mem_floatPositionHolder;
	int mem_intScreenWidth;
	int mem_intScreenHeight;
	int mem_intX;
	int mem_intY;
};

