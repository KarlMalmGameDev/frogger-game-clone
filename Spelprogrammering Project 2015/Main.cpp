#include "Engine.h"

int main(int argc, char* args[])

{
	Engine engine; 
	if (engine.Initialize())
	{
		engine.Run();
	}
	return 0;
}