#include "AudioManager.h"
#include "SDL_mixer.h"
#include <SDL.h>
//Unfinished
AudioManager::AudioManager() 
{

}


AudioManager::~AudioManager()
{
	
}
bool AudioManager::Initialize()
{
	Mix_Init(MIX_INIT_MP3);
	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024) == -1)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
		return false;
	}
	return true; 
}

void AudioManager::Shutdown()
{
	{
		auto iter = mem_arraypointextAudioClips.begin();
		while (iter != mem_arraypointextAudioClips.end())
		{
			delete (*iter);
			iter++;
		}
		mem_arraypointextAudioClips.clear();
	}
	{
		auto iter = mem_arraypointextMusicClips.begin();
		while (iter != mem_arraypointextMusicClips.end())
		{
			delete (*iter); //Deletes what is being pointed at
			iter++;
		}
		mem_arraypointextMusicClips.clear(); //Clears lists
	}
	Mix_CloseAudio();
	Mix_Quit();
}

AudioClip * AudioManager::CreateSound(const std::string& point_stringFilepath)
{
	//Checks if the sound is already loaded, if not loads it.
	auto iter = mem_arraypointextAudioChunks.find(point_stringFilepath);
	if (iter == mem_arraypointextAudioChunks.end())
	{
		
		Mix_Chunk* mem_extChunk = Mix_LoadWAV(point_stringFilepath.c_str());
		mem_arraypointextAudioChunks.insert(std::pair<std::string, Mix_Chunk*>(point_stringFilepath, mem_extChunk));
		iter = mem_arraypointextAudioChunks.find(point_stringFilepath);
	}
	AudioClip* extSound = new AudioClip((iter->second));
	return extSound;
}

MusicClip* AudioManager::CreateMusic(const std::string& point_stringFilepath)
{
	auto iter = mem_arraypointextMusicChunks.find(point_stringFilepath);
	if (iter == mem_arraypointextMusicChunks.end())
	{
		
		Mix_Music* mem_extMusic = Mix_LoadMUS(point_stringFilepath.c_str());
		mem_arraypointextMusicChunks.insert(std::pair<std::string, Mix_Music*>(point_stringFilepath, mem_extMusic));
		iter = mem_arraypointextMusicChunks.find(point_stringFilepath);
	}
	MusicClip* extMusic = new MusicClip((iter->second));
	return extMusic;
}

void AudioManager::DestroySound(const std::string& point_stringFilepath)
{

	auto iter = mem_arraypointextAudioChunks.begin();
	while (iter != mem_arraypointextAudioChunks.end())
	{
		if ((iter->first) == point_stringFilepath)
		{
			Mix_FreeChunk(iter->second);
			mem_arraypointextAudioChunks.erase(iter);
			return;
		}
		
		iter++;
	}
}

void AudioManager::DestroyMusic(const std::string& point_stringFilepath)
{
	auto iter = mem_arraypointextMusicChunks.begin();
	while (iter != mem_arraypointextMusicChunks.end())
	{
		if ((iter->first) == point_stringFilepath)
		{
			Mix_FreeMusic(iter->second);
			mem_arraypointextMusicChunks.erase(iter);
			return;
		}

		iter++;
	}
}


