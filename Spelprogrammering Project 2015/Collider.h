#pragma once
#include <SDL.h>
class IEntity;

class Collider
{
public:
	Collider(int point_intWidth, int point_intHeight);
	void SetPosition(int point_intX, int point_intY);
	void SetSize(int point_intWidth, int point_intHeight);
	int GetX();
	int GetY();
	int GetW();
	int GetH();
	void SetParent(IEntity* point_extParent); // Sets the parent Entity
	void Refresh(); // If the collider has a parent entity it will set its position to the parents position
private:
	Collider() {};
	IEntity* mem_pointextParent;
	SDL_Rect mem_extRegion;
};

