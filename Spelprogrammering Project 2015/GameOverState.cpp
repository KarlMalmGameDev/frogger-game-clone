#include "GameOverState.h"
#include <SDL.h>
#include "Sprite.h"
#include "AudioClip.h"
#include "AudioManager.h"
#include "SpriteManager.h"
#include "DrawManager.h"
#include "InputManager.h"


GameOverState::GameOverState(System& point_extSystem)
{
	mem_extpointBackgroundSprite = nullptr;
	mem_extpointLoseAudio = nullptr;
	mem_extSystem = point_extSystem;
}


GameOverState::~GameOverState()
{
}

void GameOverState::Enter()
{
	mem_extpointLoseAudio = mem_extSystem.mem_pointextAudioManager->CreateSound("../assets/GameOver.wav");
	mem_extpointLoseAudio->PlaySound();
	mem_extpointBackgroundSprite = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/FroggerLose.bmp", 0, 0, 520, 400);
}

void GameOverState::Exit()
{
	mem_extSystem.mem_pointextSpriteManager->DestroySprite(mem_extpointBackgroundSprite);
	mem_extSystem.mem_pointextAudioManager->DestroySound("../assets/GameOver.wav");
}

bool GameOverState::Update(float point_floatDeltaTime)
{
	return true;
}

void GameOverState::Draw()
{
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointBackgroundSprite, 0, 0);
}

IState * GameOverState::NextState()
{
	return nullptr;
}
