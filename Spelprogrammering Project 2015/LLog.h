#pragma once
class Player;
#include "IEntity.h"

class LLog : public IEntity
{
public:
	LLog(Sprite* point_extSprite, float point_floatX, float point_floatY, int intScreenWidth, int intScreenHeight);
	~LLog();
	void Update(float point_floatDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	float GetSpeed();
	bool IsVisible();
	EENTITYTYPE GetType();
	float mem_floatPositionHolder;

private:
	LLog() { };
	Sprite* mem_pointextSprite;
	Collider* mem_pointextCollider;
	Player* mem_pointextPlayer;
	float mem_floatX;
	float mem_floatY;
	float mem_floatspeed;
	bool mem_boolVisible;
	int mem_intScreenWidth;
	int mem_intScreenHeight;
};

