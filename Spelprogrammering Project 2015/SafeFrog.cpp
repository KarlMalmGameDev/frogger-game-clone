#include <SDL.h>
#include "SafeFrog.h"
#include "Sprite.h"
#include "Collider.h"





SafeFrog::SafeFrog(Sprite* point_extSprite, int point_intX, int point_intY, int int_ScreenWidth, int int_ScreenHeight)
{
	mem_pointextSprite = point_extSprite;
	mem_intX = point_intX;
	mem_intY = point_intY;
	mem_intScreenWidth = int_ScreenWidth;
	mem_intScreenHeight = int_ScreenHeight;
	mem_pointextCollider = new Collider(
		point_extSprite->GetRegion()->w,
		point_extSprite->GetRegion()->h);
	mem_pointextCollider->SetParent(this);
	mem_pointextCollider->Refresh();
}

SafeFrog::~SafeFrog()
{
}

void SafeFrog::Update(float point_floatDeltaTime)
{


}

Sprite* SafeFrog::GetSprite()
{
	return mem_pointextSprite;
}

Collider* SafeFrog::GetCollider()
{
	return mem_pointextCollider;
}



float SafeFrog::GetX() { return mem_intX; }

float SafeFrog::GetY() { return mem_intY; }

bool SafeFrog::IsVisible() { return mem_boolVisible; }

void SafeFrog::SetVisible(bool boolvalue)
{
	mem_boolVisible = boolvalue;
}

EENTITYTYPE SafeFrog::GetType() { return ENTITY_SAFEFROG; } 