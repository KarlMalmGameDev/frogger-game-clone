#pragma once
class SpriteManager;
class DrawManager;
class InputManager;
class AudioManager;

struct System
{
	int mem_intScreenWidth;
	int mem_intScreenHeight;
	SpriteManager* mem_pointextSpriteManager;
	DrawManager* mem_pointextDrawManager;
	InputManager* mem_pointextInputManager;
	AudioManager* mem_pointextAudioManager;
};
class IState
{
public:
	virtual ~IState() {};
	virtual void Enter() {};
	virtual bool Update(float point_floatDeltaTime) = 0;
	virtual void Exit() {};
	virtual void Draw() = 0;
	virtual IState* NextState() = 0;
};

