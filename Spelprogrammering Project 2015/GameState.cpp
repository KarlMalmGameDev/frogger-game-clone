#include "GameState.h" 
#include <SDL.h>
#include "SpriteManager.h"
#include "DrawManager.h"
#include "CollisionManager.h"
#include "AudioManager.h"
#include "AnimatedSprite.h"
#include "InputManager.h"
#include "Sprite.h"
#include "AudioClip.h" 
#include "MusicClip.h" 
#include "Istate.h"
#include "Player.h"
#include "SafeFrog.h"
#include "RLog.h"
#include "LLog.h"
#include "LCar.h"
#include "WinState.h"
#include "GameOverState.h"
#include <math.h>






GameState::GameState(System& point_extSystem)
{
	frogsLeft = 5;

//Sprites Nulled
mem_extpointBackgroundSprite = nullptr;
mem_extpointPlayerFrogSprite = nullptr;
mem_extpointPlayerFrogSprite1 = nullptr;
mem_extpointPlayerFrogSprite2 = nullptr;
mem_extpointPlayerFrogSprite3 = nullptr;
mem_extpointSafeFrogSprite = nullptr;
mem_extpointLogSprite = nullptr;
mem_extpointLCarSprite = nullptr;
mem_extpointLCarSprite1 = nullptr;
mem_extpointLCarTruckSprite = nullptr;
mem_extpointOneUpSprite = nullptr;

//Audio Nulled
mem_extpointSplatClip = nullptr;
mem_extpointCollisionClip = nullptr;
mem_extpointDrowningClip = nullptr;
mem_extpointGoalClip = nullptr;
mem_extpointTheme = nullptr;


mem_extSystem = point_extSystem;

}

GameState::~GameState()
{

}

void GameState::Enter()
{
	//Sprite Assignment
	 mem_extpointBackgroundSprite = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/FroggerBack.png" ,0, 0, 520, 400);
	 mem_extpointPlayerFrogSprite = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/spritesheet.png", 19, 19, 20, 20);
	 mem_extpointPlayerFrogSprite1 = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/spritesheet.png", 106, 20, 20, 20);
	 mem_extpointPlayerFrogSprite2 = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/spritesheet.png", 189, 21, 20, 20);
	 mem_extpointPlayerFrogSprite3 = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/spritesheet.png", 218, 23, 20, 20);
	 mem_extpointOneUpSprite = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/spritesheet.png", 729, 19, 10, 10);
	 mem_extpointSafeFrogSprite = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/spritesheet.png", 640, 22, 20, 30);
	 mem_extpointLogSprite = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/spritesheet.png", 596, 366, 54, 20);
	 mem_extpointLCarSprite = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/spritesheet.png", 19 ,404 ,20 ,20 );
	 mem_extpointLCarSprite1 = mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/spritesheet.png", 48, 404, 20, 20);
	 mem_extpointLCarTruckSprite= mem_extSystem.mem_pointextSpriteManager->CreateSprite("../assets/spritesheet.png", 80, 404, 36, 15);

	 //Audio Clip Setup
	 mem_extpointSplatClip = mem_extSystem.mem_pointextAudioManager->CreateSound("../assets/CarSplat.wav");
	 mem_extpointCollisionClip = mem_extSystem.mem_pointextAudioManager->CreateSound("../assets/Collision.wav");
	 mem_extpointDrowningClip = mem_extSystem.mem_pointextAudioManager->CreateSound("../assets/Drowning.wav");
	 mem_extpointGoalClip = mem_extSystem.mem_pointextAudioManager->CreateSound("../assets/Goal.wav");

	 //Music Clip Setup
	 mem_extpointTheme = mem_extSystem.mem_pointextAudioManager->CreateMusic("../assets/FroggerTheme.mp3");
	 mem_extpointTheme->PlayMusic(-1);

	 //Player Setup
	 SDL_Rect* extRect = mem_extpointPlayerFrogSprite->GetRegion();
	 int intHeight = extRect->h;
	 int intWidth = extRect->w;
	 mem_extpointPlayer = new Player(mem_extSystem.mem_pointextInputManager,
		 mem_extpointPlayerFrogSprite, mem_extSystem.mem_intScreenWidth / 2,
		 mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h,
		 mem_extSystem.mem_intScreenHeight, mem_extSystem.mem_intScreenWidth);
	 
	 //SafeFrogs Setup 
	 for (int i = 0; i < 6; i++)
	 {
		 SafeFrog frog = SafeFrog(mem_extpointSafeFrogSprite, safex, safey, mem_extSystem.mem_intScreenHeight, mem_extSystem.mem_intScreenWidth);
		 mem_extvectSafeFrogs.push_back(frog);
		 mem_extvectRects.emplace_back(mem_extpointSafeFrogSprite->GetRegion());
		 safex += 110;
	 }
	 /*
	 mem_extpointSafeFrog = new SafeFrog(mem_extpointSafeFrogSprite,
		 25, 50,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect37 = mem_extpointSafeFrogSprite->GetRegion();

	 mem_extpointSafeFrog1 = new SafeFrog(mem_extpointSafeFrogSprite,
		 135, 50,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect38 = mem_extpointSafeFrogSprite->GetRegion();

	 mem_extpointSafeFrog2 = new SafeFrog(mem_extpointSafeFrogSprite,
		 250, 50,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect39 = mem_extpointSafeFrogSprite->GetRegion();

	 mem_extpointSafeFrog3 = new SafeFrog(mem_extpointSafeFrogSprite,
		 360, 50,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect40 = mem_extpointSafeFrogSprite->GetRegion();

	 mem_extpointSafeFrog4 = new SafeFrog(mem_extpointSafeFrogSprite,
		 470, 50,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect41 = mem_extpointSafeFrogSprite->GetRegion();
	 */

	 //LCar Setup 

	 //Sports Cars
	 for (int i=0; i < 3; i++)
	 {
		 LCar sport = LCar(mem_extpointLCarSprite, sportsx, sportsy, mem_extSystem.mem_intScreenHeight, mem_extSystem.mem_intScreenHeight);
		 mem_extvectLCars.push_back(sport);
		 mem_extvectRects.emplace_back (mem_extpointLCarSprite->GetRegion());
		 sportsx-=100;
		 sportsy+=40;
	 }
	 //Regular Cars
	 for (int i = 0; i < 3; i++)
	 {
		 LCar reg = LCar(mem_extpointLCarSprite1, regx, regy, mem_extSystem.mem_intScreenHeight, mem_extSystem.mem_intScreenHeight);
		 mem_extvectLCars.push_back(reg);
		 mem_extvectRects.emplace_back(mem_extpointLCarSprite->GetRegion());
		 regx += 100;
		 regy += 30;
	 }
	 //Trucks
	 for (int i = 0; i < 3; i++)
	 {
		 LCar truck = LCar(mem_extpointLCarTruckSprite, truckx, trucky, mem_extSystem.mem_intScreenHeight, mem_extSystem.mem_intScreenHeight);
		 mem_extvectLCars.push_back(truck);
		 mem_extvectRects.emplace_back(mem_extpointLCarSprite->GetRegion());
		 regx += 150;
		 regy += 40;
	 }
	 /*
	 mem_extpointLeftCar = new LCar(mem_extpointLCarSprite,
		 300, 230, 
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect28 = mem_extpointLCarSprite->GetRegion();

	 mem_extpointLeftCar5 = new LCar(mem_extpointLCarSprite,
		 200, 270,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect33 = mem_extpointLCarSprite->GetRegion();

	 mem_extpointLeftCar6 = new LCar(mem_extpointLCarSprite,
		 100, 310,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect34 = mem_extpointLCarSprite->GetRegion();
	//Regular Cars

	 mem_extpointLeftCar2 = new LCar(mem_extpointLCarSprite1,
		 50, 260,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect30 = mem_extpointLCarSprite1->GetRegion();

	 mem_extpointLeftCar3 = new LCar(mem_extpointLCarSprite1,
		 150, 290,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect31 = mem_extpointLCarSprite1->GetRegion();

	 mem_extpointLeftCar4 = new LCar(mem_extpointLCarSprite1,
		 250, 320,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect32 = mem_extpointLCarSprite1->GetRegion();
	 //Trucks
	 mem_extpointLeftCar1 = new LCar(mem_extpointLCarTruckSprite,
		 150, 230,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect29 = mem_extpointLCarTruckSprite->GetRegion();
	 mem_extpointLeftCar7 = new LCar(mem_extpointLCarTruckSprite,
		 300, 270,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect35 = mem_extpointLCarTruckSprite->GetRegion();
	 mem_extpointLeftCar8 = new LCar(mem_extpointLCarTruckSprite,
		 0, 330,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect36 = mem_extpointLCarTruckSprite->GetRegion();
	 */
	 //RLogs Setup
	 for (int i=0; i < 19; i++)
	 {
		 RLog rl = RLog(mem_extpointLogSprite, rlogx, rlogy, mem_extSystem.mem_intScreenHeight, mem_extSystem.mem_intScreenWidth);
		 mem_extvectRLog.push_back(rl);
		 mem_extvectRects.emplace_back(mem_extpointLCarSprite->GetRegion());
		 if (i % 3 == 0)
		 {
			 rlogx += 108;
			 rlogy = 80;
		 }
		 else
		 {
			 rlogy += 40;
		 }
	 }
	 /*
	 mem_extpointRightLog = new RLog(mem_extpointLogSprite,
		 -100, 80,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect1 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog1 = new RLog(mem_extpointLogSprite,
		 -100, 120,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect2 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog2 = new RLog(mem_extpointLogSprite,
		 -100, 160,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect3 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog3 = new RLog(mem_extpointLogSprite,
		 8, 80,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect4 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog4 = new RLog(mem_extpointLogSprite,
		 8, 120,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect5 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog5 = new RLog(mem_extpointLogSprite,
		 8, 160,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect6 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog6 = new RLog(mem_extpointLogSprite,
		 114, 80,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect7 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog7 = new RLog(mem_extpointLogSprite,
		 114, 120,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect8 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog8 = new RLog(mem_extpointLogSprite,
		 114, 160,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect9 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog9 = new RLog(mem_extpointLogSprite,
		 222, 80,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect10 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog10 = new RLog(mem_extpointLogSprite,
		 222, 120,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect11 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog11 = new RLog(mem_extpointLogSprite,
		 222, 160,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect12 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog12 = new RLog(mem_extpointLogSprite,
		 328,80,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect13 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog13 = new RLog(mem_extpointLogSprite,

		 328, 120,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect14 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog14 = new RLog(mem_extpointLogSprite,
		 328,160,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect15 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog15 = new RLog(mem_extpointLogSprite,
		 436, 80,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect16 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog16 = new RLog(mem_extpointLogSprite,
		 436, 120,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect17 = mem_extpointLogSprite->GetRegion();

	 mem_extpointRightLog17 = new RLog(mem_extpointLogSprite,
		 436, 160,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect18 = mem_extpointLogSprite->GetRegion();
	 */
	 //
	 //LLogs Setup
	 for (int i = 0; i < 10;i++)
	 {
		 LLog ll = LLog(mem_extpointLogSprite, 600 - 158 * (i % 3), 175 - 35 * floor(i / 3), mem_extSystem.mem_intScreenHeight, mem_extSystem.mem_intScreenWidth);
		 mem_extvectLLogs.push_back(ll);
		 mem_extvectRects.emplace_back(mem_extpointLogSprite->GetRegion());
		
	 }
	 /*
	 mem_extpointLeftLog = new LLog(mem_extpointLogSprite,
		 600, 175,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect19 = mem_extpointLogSprite->GetRegion();

	 mem_extpointLeftLog1 = new LLog(mem_extpointLogSprite,
		 442, 175,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect20 = mem_extpointLogSprite->GetRegion();

	 mem_extpointLeftLog2 = new LLog(mem_extpointLogSprite,
		 284, 175,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect21 = mem_extpointLogSprite->GetRegion();

	 mem_extpointLeftLog3 = new LLog(mem_extpointLogSprite,
		 600, 140,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect22 = mem_extpointLogSprite->GetRegion();

	 mem_extpointLeftLog4 = new LLog(mem_extpointLogSprite,
		 442, 140,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect23 = mem_extpointLogSprite->GetRegion();

	 mem_extpointLeftLog5 = new LLog(mem_extpointLogSprite,
		 284, 140,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect24 = mem_extpointLogSprite->GetRegion();

	 mem_extpointLeftLog6 = new LLog(mem_extpointLogSprite,
		 600, 100,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect25 = mem_extpointLogSprite->GetRegion();

	 mem_extpointLeftLog7 = new LLog(mem_extpointLogSprite,
		 442, 100,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect26 = mem_extpointLogSprite->GetRegion();

	 mem_extpointLeftLog8 = new LLog(mem_extpointLogSprite,
		 284, 100,
		 mem_extSystem.mem_intScreenHeight,
		 mem_extSystem.mem_intScreenWidth);
	 SDL_Rect* extRect27 = mem_extpointLogSprite->GetRegion();
	 */
}
void GameState::Exit()
{

	
mem_extpointTheme->StopMusic();
mem_extSystem.mem_pointextAudioManager->DestroyMusic("../assets/FroggerTheme.mp3");
mem_extSystem.mem_pointextAudioManager->DestroySound("../assets/CarSplat.wav");
mem_extSystem.mem_pointextAudioManager->DestroySound("../assets/Collision.wav");
mem_extSystem.mem_pointextAudioManager->DestroySound("../assets/Drowning.wav");
mem_extSystem.mem_pointextAudioManager->DestroySound("../assets/Goal.wav");
mem_extSystem.mem_pointextSpriteManager->DestroySprite(mem_extpointBackgroundSprite);


mem_extSystem.mem_pointextSpriteManager->DestroySprite(mem_extpointLogSprite);


mem_extSystem.mem_pointextSpriteManager->DestroySprite(mem_extpointLCarSprite);

mem_extSystem.mem_pointextSpriteManager->DestroySprite(mem_extpointLCarSprite1);

mem_extSystem.mem_pointextSpriteManager->DestroySprite(mem_extpointLCarTruckSprite);

mem_extSystem.mem_pointextSpriteManager->DestroySprite(mem_extpointOneUpSprite);

}

bool GameState::Update(float point_floatDeltaTime)
{
	mem_extpointPlayer->Update(point_floatDeltaTime);
	if (frogsLeft == 0)
	{
		return false;
	}
	else if (mem_extpointPlayer->mem_intOneUps == 0)
	{
		return false;
	}
	//SafeFrog Update
	for (int i = 0; i < 6; i++)
	{
		mem_extvectSafeFrogs[i].Update(point_floatDeltaTime);
	}
	//Updates Cars 
	for (int i = 0; i < 9; i++)
	{
		mem_extvectLCars[i].Update(point_floatDeltaTime);
	}
	/*
	mem_extpointLeftCar->Update(point_floatDeltaTime);
	mem_extpointLeftCar1->Update(point_floatDeltaTime);
	mem_extpointLeftCar2->Update(point_floatDeltaTime);
	mem_extpointLeftCar3->Update(point_floatDeltaTime);
	mem_extpointLeftCar4->Update(point_floatDeltaTime);
	mem_extpointLeftCar5->Update(point_floatDeltaTime);
	mem_extpointLeftCar6->Update(point_floatDeltaTime);
	mem_extpointLeftCar7->Update(point_floatDeltaTime);
	mem_extpointLeftCar8->Update(point_floatDeltaTime);
	*/
	//Updates Logs
	for (int i = 0; i < 17; i++)
	{
		mem_extvectRLog[i].Update(point_floatDeltaTime);
	}
	/*
	mem_extpointRightLog->Update(point_floatDeltaTime);
	mem_extpointRightLog1->Update(point_floatDeltaTime);
	mem_extpointRightLog2->Update(point_floatDeltaTime);
	mem_extpointRightLog3->Update(point_floatDeltaTime);
	mem_extpointRightLog4->Update(point_floatDeltaTime);
	mem_extpointRightLog5->Update(point_floatDeltaTime);
	mem_extpointRightLog6->Update(point_floatDeltaTime);
	mem_extpointRightLog7->Update(point_floatDeltaTime);
	mem_extpointRightLog8->Update(point_floatDeltaTime);
	mem_extpointRightLog9->Update(point_floatDeltaTime);
	mem_extpointRightLog10->Update(point_floatDeltaTime);
	mem_extpointRightLog11->Update(point_floatDeltaTime);
	mem_extpointRightLog12->Update(point_floatDeltaTime);
	mem_extpointRightLog13->Update(point_floatDeltaTime);
	mem_extpointRightLog14->Update(point_floatDeltaTime);
	mem_extpointRightLog15->Update(point_floatDeltaTime);
	mem_extpointRightLog16->Update(point_floatDeltaTime);
	mem_extpointRightLog17->Update(point_floatDeltaTime);
	*/
	for (int i = 0; i < 9; i++)
	{
		mem_extvectLLogs[i].Update(point_floatDeltaTime);
	}
	/*
	mem_extpointLeftLog->Update(point_floatDeltaTime);
	mem_extpointLeftLog1->Update(point_floatDeltaTime);
	mem_extpointLeftLog2->Update(point_floatDeltaTime);
	mem_extpointLeftLog3->Update(point_floatDeltaTime);
	mem_extpointLeftLog4->Update(point_floatDeltaTime);
	mem_extpointLeftLog5->Update(point_floatDeltaTime);
	mem_extpointLeftLog6->Update(point_floatDeltaTime);
	mem_extpointLeftLog7->Update(point_floatDeltaTime);
	mem_extpointLeftLog8->Update(point_floatDeltaTime);
	*/
	/*
	mem_extpointSafeFrog->Update(point_floatDeltaTime);
	mem_extpointSafeFrog1->Update(point_floatDeltaTime);
	mem_extpointSafeFrog2->Update(point_floatDeltaTime);
	mem_extpointSafeFrog3->Update(point_floatDeltaTime);
	mem_extpointSafeFrog4->Update(point_floatDeltaTime);
	*/
	CheckCollision();

	return true; 
}
void GameState::Draw()
{

	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointBackgroundSprite,0,0);

	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	
	//Logs
	for (int i = 0; i < 18; i++)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extvectRLog[i].GetX(), mem_extvectRLog[i].GetY());
	}
	/*
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog->GetX(), mem_extpointRightLog->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog1->GetX(), mem_extpointRightLog1->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog2->GetX(), mem_extpointRightLog2->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog3->GetX(), mem_extpointRightLog3->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog4->GetX(), mem_extpointRightLog4->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog5->GetX(), mem_extpointRightLog5->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog6->GetX(), mem_extpointRightLog6->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog7->GetX(), mem_extpointRightLog7->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog8->GetX(), mem_extpointRightLog8->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog9->GetX(), mem_extpointRightLog9->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog10->GetX(), mem_extpointRightLog10->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog11->GetX(), mem_extpointRightLog11->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog12->GetX(), mem_extpointRightLog12->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog13->GetX(), mem_extpointRightLog13->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog14->GetX(), mem_extpointRightLog14->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog15->GetX(), mem_extpointRightLog15->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog16->GetX(), mem_extpointRightLog16->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointRightLog17->GetX(), mem_extpointRightLog17->GetY());
	*/

	for (int i = 0; i < 9; i++)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extvectLLogs[i].GetX(), mem_extvectLLogs[i].GetY());
	}
	/*
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointLeftLog->GetX(), mem_extpointLeftLog->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointLeftLog1->GetX(), mem_extpointLeftLog1->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointLeftLog2->GetX(), mem_extpointLeftLog2->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointLeftLog3->GetX(), mem_extpointLeftLog3->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointLeftLog4->GetX(), mem_extpointLeftLog4->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointLeftLog5->GetX(), mem_extpointLeftLog5->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointLeftLog6->GetX(), mem_extpointLeftLog6->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointLeftLog7->GetX(), mem_extpointLeftLog7->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLogSprite, mem_extpointLeftLog8->GetX(), mem_extpointLeftLog8->GetY());
	*/

	//Cars 
	for (int i = 0; i < 9; i++)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLCarSprite, mem_extvectLCars[i].GetX(), mem_extvectLCars[i].GetY());
	}
	/*
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLCarSprite, mem_extpointLeftCar->GetX(), mem_extpointLeftCar->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLCarSprite, mem_extpointLeftCar5->GetX(), mem_extpointLeftCar5->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLCarSprite, mem_extpointLeftCar6->GetX(), mem_extpointLeftCar6->GetY());

	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLCarTruckSprite, mem_extpointLeftCar1->GetX(), mem_extpointLeftCar1->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLCarTruckSprite, mem_extpointLeftCar7->GetX(), mem_extpointLeftCar7->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLCarTruckSprite, mem_extpointLeftCar8->GetX(), mem_extpointLeftCar8->GetY());

	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLCarSprite1, mem_extpointLeftCar2->GetX(), mem_extpointLeftCar2->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLCarSprite1, mem_extpointLeftCar3->GetX(), mem_extpointLeftCar3->GetY());
	mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointLCarSprite1, mem_extpointLeftCar4->GetX(), mem_extpointLeftCar4->GetY());
	*/
	//Player Direction Drawing

	if (mem_extpointPlayer->mem_intDirection == 1)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}
	else if (mem_extpointPlayer->mem_intDirection==2)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite1, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}
	else if (mem_extpointPlayer->mem_intDirection == 3)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite2, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}
	else if (mem_extpointPlayer->mem_intDirection == 4)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite3, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}

	//SafeFrogs
	for (int i = 0; i < 6; i++)
	{
		if (mem_extvectSafeFrogs[i].IsVisible())
		{
			mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointSafeFrogSprite, mem_extvectSafeFrogs[i].GetX(), mem_extvectSafeFrogs[i].GetY());
		}

	}
	/*
	if (mem_extpointSafeFrog->IsVisible() == true)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointSafeFrogSprite, mem_extpointSafeFrog->GetX(), mem_extpointSafeFrog->GetY());
	}

	if (mem_extpointSafeFrog1->IsVisible() == true)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointSafeFrogSprite, mem_extpointSafeFrog1->GetX(), mem_extpointSafeFrog1->GetY());
	}

	if (mem_extpointSafeFrog2->IsVisible() == true)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointSafeFrogSprite, mem_extpointSafeFrog2->GetX(), mem_extpointSafeFrog2->GetY());
	}

	if (mem_extpointSafeFrog3->IsVisible() == true)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointSafeFrogSprite, mem_extpointSafeFrog3->GetX(), mem_extpointSafeFrog3->GetY());
	}

	if (mem_extpointSafeFrog4->IsVisible() == true)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointSafeFrogSprite, mem_extpointSafeFrog4->GetX(), mem_extpointSafeFrog4->GetY());
	}
	*/

	//OneUp Draw
	for (int i = 0; i < mem_extpointPlayer->mem_intOneUps - 1; i++)
	{
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointOneUpSprite, i * 25, 20);
	}
}
void GameState::CheckCollision()
{
	int intOverlapX = 0;
	int intOverlapY = 0;

	//SafeFrog Collision, Scoring and Walls
	for (int i = 0; i < 6; i++)
	{
		if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extvectSafeFrogs[i].GetCollider(), intOverlapX, intOverlapY));
		{
			if (mem_extvectSafeFrogs[i].IsVisible() == false)
			{
				mem_extpointGoalClip->PlaySound();
				frogsLeft--;
				mem_extvectSafeFrogs[i].SetVisible(true);
				mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
				mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
				mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
			}
			else if (mem_extvectSafeFrogs[i].IsVisible() == true)
			{
				mem_extpointCollisionClip->PlaySound();
				mem_extpointPlayer->mem_floatY += 20;
			}
		}
	}
	//Car Collisions 
	for (int i=0; i < 9; i++)
	{
		if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extvectLCars[i].GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointSplatClip->PlaySound();
			mem_extpointPlayer->mem_intOneUps--;
			mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
			mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
			mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
		}
	}
	/*
	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftCar->GetCollider(), intOverlapX, intOverlapY))
	{
		mem_extpointSplatClip->PlaySound(); 
		mem_extpointPlayer->mem_intOneUps--;
		mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
		mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftCar1->GetCollider(), intOverlapX, intOverlapY))
	{
		mem_extpointSplatClip->PlaySound();
		mem_extpointPlayer->mem_intOneUps--;
		mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
		mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftCar2->GetCollider(), intOverlapX, intOverlapY))
	{
		mem_extpointSplatClip->PlaySound();
		mem_extpointPlayer->mem_intOneUps--;
		mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
		mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftCar3->GetCollider(), intOverlapX, intOverlapY))
	{
		mem_extpointSplatClip->PlaySound();
		mem_extpointPlayer->mem_intOneUps--;
		mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
		mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftCar4->GetCollider(), intOverlapX, intOverlapY))
	{
		mem_extpointSplatClip->PlaySound();
		mem_extpointPlayer->mem_intOneUps--;
		mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
		mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftCar5->GetCollider(), intOverlapX, intOverlapY))
	{
		mem_extpointSplatClip->PlaySound();
		mem_extpointPlayer->mem_intOneUps--;
		mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
		mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftCar6->GetCollider(), intOverlapX, intOverlapY))
	{
		mem_extpointSplatClip->PlaySound();
		mem_extpointPlayer->mem_intOneUps--;
		mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
		mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftCar7->GetCollider(), intOverlapX, intOverlapY))
	{
		mem_extpointSplatClip->PlaySound();
		mem_extpointPlayer->mem_intOneUps--;
		mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
		mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftCar8->GetCollider(), intOverlapX, intOverlapY))
	{
		mem_extpointSplatClip->PlaySound();
		mem_extpointPlayer->mem_intOneUps--;
		mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
		mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
		mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
	}
	*/

	//Right and left-moving Logs Collision, moves player along with logs, if not on a log and above the Y value 200, loses a life
	if (mem_extpointPlayer->GetY() < 200)
	{
		isDrowned = true; //Preliminary setting the frog as drowned, will not drown if colliding with a log
		for (int i = 0; i < 18; i++)
		{
			if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extvectRLog[i].GetCollider(), intOverlapX, intOverlapY))
			{
				isDrowned = false;
				mem_extpointPlayer->mem_floatX++;
			}
			else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extvectLLogs[i].GetCollider(), intOverlapX, intOverlapY))
			{
				isDrowned = false;
				mem_extpointPlayer->mem_floatX -= (mem_extvectLLogs[i].GetSpeed());
			}

			/*
		}
		//OLD CODE REMOVE AND REDO
		if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}

		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog1->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog2->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog3->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog4->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog5->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog6->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog7->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog8->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog9->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog10->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog11->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog12->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog13->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog14->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog15->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog16->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointRightLog17->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX++;
		}
		//Left-moving Logs Collision
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftLog->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX--;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftLog2->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX--;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftLog3->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX -= 1.1f;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftLog4->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX -= 1.1f;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftLog5->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX -= 1.1f;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftLog6->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX -= 1.2f;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftLog7->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX -= 1.2f;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftLog8->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX -= 1.2f;
		}
		else if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointLeftLog1->GetCollider(), intOverlapX, intOverlapY))
		{
			mem_extpointPlayer->mem_floatX--;
		}
		*/

			if (mem_extpointPlayer->mem_floatY > 70 && isDrowned == true)
			{
				mem_extpointDrowningClip->PlaySound();
				mem_extpointPlayer->mem_intOneUps--;
				mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
				mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
				mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
			}
		}
	}


	/*
	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointSafeFrog->GetCollider(), intOverlapX, intOverlapY))
	{
		if (mem_extpointSafeFrog->IsVisible() == false)
		{
			mem_extpointGoalClip->PlaySound();
			frogsLeft--;
			mem_extpointSafeFrog->SetVisible(true);
			mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
			mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
			mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
		}


		else if (mem_extpointSafeFrog->IsVisible() == true)
		{
			mem_extpointCollisionClip->PlaySound();
			mem_extpointPlayer->mem_floatY += 20;
		}


	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointSafeFrog1->GetCollider(), intOverlapX, intOverlapY))
	{
		if (mem_extpointSafeFrog1->IsVisible() == false)
		{
			mem_extpointGoalClip->PlaySound();
			frogsLeft--;
			mem_extpointSafeFrog1->SetVisible(true);
			mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
			mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
			mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
		}


		else if (mem_extpointSafeFrog1->IsVisible() == true)
		{
			mem_extpointCollisionClip->PlaySound();
			mem_extpointPlayer->mem_floatY += 20;
		}
	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointSafeFrog2->GetCollider(), intOverlapX, intOverlapY))
	{
		if (mem_extpointSafeFrog2->IsVisible() == false)
		{
			mem_extpointGoalClip->PlaySound();
			frogsLeft--;
			mem_extpointSafeFrog2->SetVisible(true);
			mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
			mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
			mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
		}


		else if (mem_extpointSafeFrog2->IsVisible() == true)
		{
			mem_extpointCollisionClip->PlaySound();
			mem_extpointPlayer->mem_floatY += 20;
		}
	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointSafeFrog3->GetCollider(), intOverlapX, intOverlapY))
	{
		if (mem_extpointSafeFrog3->IsVisible() == false)
		{
			mem_extpointGoalClip->PlaySound();
			frogsLeft--;
			mem_extpointSafeFrog3->SetVisible(true);
			mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
			mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
			mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
		}


		else if (mem_extpointSafeFrog3->IsVisible() == true)
		{
			mem_extpointCollisionClip->PlaySound();
			mem_extpointPlayer->mem_floatY += 20;
		}
	}

	if (CollisionManager::Check(mem_extpointPlayer->GetCollider(), mem_extpointSafeFrog4->GetCollider(), intOverlapX, intOverlapY))
	{
		if (mem_extpointSafeFrog4->IsVisible() == false)
		{
			mem_extpointGoalClip->PlaySound();
			frogsLeft--;
			mem_extpointSafeFrog4->SetVisible(true);
			mem_extpointPlayer->mem_floatX = mem_extSystem.mem_intScreenWidth / 2;
			mem_extpointPlayer->mem_floatY = mem_extSystem.mem_intScreenHeight - mem_extpointPlayerFrogSprite->GetRegion()->h;
			mem_extSystem.mem_pointextDrawManager->Draw(mem_extpointPlayerFrogSprite, mem_extpointPlayer->GetX(), mem_extpointPlayer->GetY());
		}


		else if (mem_extpointSafeFrog4->IsVisible() == true)
		{
			mem_extpointCollisionClip->PlaySound();
			mem_extpointPlayer->mem_floatY += 20;
		}
	}
	*/
	//Wall Collision Check
	if (mem_extpointPlayer->mem_floatY < 70)
	{
		mem_extpointCollisionClip->PlaySound();
		mem_extpointPlayer->mem_floatY += 20;
	}
}

IState * GameState::NextState()
{
	if (frogsLeft == 0 && mem_extpointPlayer->mem_intOneUps>0)
	{
		return new WinState(mem_extSystem);
	}
	else if (mem_extpointPlayer->mem_intOneUps == 0)
	{
		return new GameOverState(mem_extSystem);
	}
}


