#include "RLog.h"
#include <SDL.h>
#include "Player.h"
#include "Sprite.h"
#include "Collider.h"

RLog::RLog(Sprite* point_extSprite, int point_intX, int point_intY, int point_intScreenWidth, int point_intScreenHeight)
{
	mem_pointextSprite = point_extSprite;
	mem_intX = point_intX;
	mem_intY = point_intY;
	mem_intScreenWidth = point_intScreenWidth;
	mem_intScreenHeight = point_intScreenHeight;
	mem_pointextCollider = new Collider(
		point_extSprite->GetRegion()->w,
		point_extSprite->GetRegion()->h);
	mem_pointextCollider->SetParent(this);
	mem_pointextCollider->Refresh();
	mem_floatPositionHolder = 0.0f;
}

RLog::~RLog()
{

}

void RLog::Update(float point_floatDeltaTime)
{
	mem_floatPositionHolder += point_floatDeltaTime;

	if (mem_floatPositionHolder > 0.01f)
	{
		mem_intX ++;
		mem_floatPositionHolder = 0;
	}


	if (mem_intX > 520)
	{
		mem_intX = -100;
	}

	mem_pointextCollider->Refresh();
}

Sprite* RLog::GetSprite()
{
	return mem_pointextSprite;
}

Collider* RLog::GetCollider()
{
	return mem_pointextCollider;
}

float RLog::GetX() { return mem_intX; }

float RLog::GetY() { return mem_intY; }

bool RLog::IsVisible() { return mem_boolVisible; }

EENTITYTYPE RLog::GetType() { return ENTITY_RLOG; }