#include "AnimatedSprite.h"
#include "Sprite.h"


AnimatedSprite::AnimatedSprite(SDL_Texture* point_extTexture) : Sprite(point_extTexture)
{
	mem_intIndex = 0;
	mem_floatCurrentDuration = 0.0f;
}

void AnimatedSprite::AddFrame(int point_intX, int point_intY,
	int point_intWidth, int point_intHeight, float point_floatDuration)
{
	FrameData data;
	data.mem_extRegion.x = point_intX;
	data.mem_extRegion.y = point_intY;
	data.mem_extRegion.w = point_intWidth;
	data.mem_extRegion.h = point_intHeight;
	data.mem_floatDuration = point_floatDuration;
	mem_arrayextFrames.push_back(data);
}

void AnimatedSprite::Update(float point_floatDeltaTime)
{
	// We increase the current duration with deltatime, the time from last fram to this frame.
	mem_floatCurrentDuration += point_floatDeltaTime;

	// Check if currentduration has passed our currents frames duration
	if (mem_floatCurrentDuration >= mem_arrayextFrames[mem_intIndex].mem_floatDuration)
	{
		// If the index is lower then the amount of frames counted as index (-1)
		// then we can increment the frame we will be drawing
		if (mem_intIndex < mem_arrayextFrames.size() - 1)
		{
			mem_intIndex++;
		}
		else
		{
			mem_intIndex = 0;
		}

		// Update the value of the region we will be using to the current frames region
		// and reset the current duration
		mem_extRegion = mem_arrayextFrames[mem_intIndex].mem_extRegion;
		mem_floatCurrentDuration = 0.0f;
	}
}